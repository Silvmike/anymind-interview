## Public wallet

You add transactions, it shows history.

### Run
```
mvnw spring-boot:run
```

### Usage example

```sh

# Add new transactions
curl -k -X POST 'https://localhost:8080/advance' -d '{
	"datetime": "2021-01-05T14:00:05+07:00",
	"amount": "1.02"
}'

# Fetch history
curl -k -X POST 'http://localhost:8080/history' -d '{
	"startDatetime": "2021-01-05T10:00:01+00:00",
    "endDatetime": "2021-01-05T18:00:02+00:00"
}'
```

### Existing profiles

* **STUB**

Uses tree-map based in-memory storage.
This profile allows advance message reordering.

* **H2_TRIGGER** (DEFAULT)

Uses (persistent) H2 with summarizing trigger.
This profile considers reodered messages as events that happened later.

* **H2_TRIGGER_STUB**

Uses in-memory H2 with summarizing trigger.
This profile considers reodered messages as events that happened later.