package ru.silvmike.interview.anymind.public_wallet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import ru.silvmike.interview.anymind.public_wallet.rest.WalletRest;
import ru.silvmike.interview.anymind.public_wallet.rest.converter.AdvanceRequestConverter;
import ru.silvmike.interview.anymind.public_wallet.rest.converter.Converter;
import ru.silvmike.interview.anymind.public_wallet.rest.converter.WalletHistoryAdvanceHistoryConverter;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.Advance;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.AdvanceHistory;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.HistoryRequest;
import ru.silvmike.interview.anymind.public_wallet.rest.error.advice.ValidationControllerAdvice;
import ru.silvmike.interview.anymind.public_wallet.rest.validation.AdvanceValidator;
import ru.silvmike.interview.anymind.public_wallet.rest.validation.HistoryRequestValidator;
import ru.silvmike.interview.anymind.public_wallet.rest.validation.Validator;
import ru.silvmike.interview.anymind.public_wallet.service.WalletService;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.dto.WalletHistory;

@Configuration
public class RestConfiguration {

    private static final int TEN_KB = 10 * 1024;

    @Bean
    public WalletRest walletRest(
        WalletService walletService,
        Converter<Advance, AdvanceRequest> advanceRequestConverter,
        Converter<AdvanceHistory, WalletHistory> walletHistoryConverter,
        Validator<Advance> advanceValidator,
        Validator<HistoryRequest> historyRequestValidator) {

        return new WalletRest(
            walletService,
            advanceRequestConverter, walletHistoryConverter,
            advanceValidator, historyRequestValidator
        );
    }

    @Bean
    public Converter<Advance, AdvanceRequest> advanceRequestConverter() {
        return new AdvanceRequestConverter();
    }

    @Bean
    public Converter<AdvanceHistory, WalletHistory> walletHistoryConverter(
            Converter<Advance, AdvanceRequest> advanceRequestConverter) {

        return new WalletHistoryAdvanceHistoryConverter(advanceRequestConverter);
    }

    @Bean
    public ValidationControllerAdvice validationControllerAdvice() {
        return new ValidationControllerAdvice();
    }

    @Bean
    public Validator<Advance> advanceValidator() {
        return new AdvanceValidator();
    }

    @Bean
    public Validator<HistoryRequest> historyRequestValidator() {
        return new HistoryRequestValidator();
    }

    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludeQueryString(true);
        loggingFilter.setIncludePayload(true);
        loggingFilter.setMaxPayloadLength(TEN_KB);
        return loggingFilter;
    }
}
