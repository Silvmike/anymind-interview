package ru.silvmike.interview.anymind.public_wallet.app;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import ru.silvmike.interview.anymind.public_wallet.Profiles;
import ru.silvmike.interview.anymind.public_wallet.config.RootConfiguration;

@SpringBootConfiguration
@EnableAutoConfiguration
public class Application {

	public static void main(String[] args) {

		new SpringApplicationBuilder()
				.sources(Application.class, RootConfiguration.class)
				.profiles(Profiles.H2_TRIGGER)
				.build()
			.run(args);
	}

}
