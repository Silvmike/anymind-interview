package ru.silvmike.interview.anymind.public_wallet.service.h2_trigger;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class TruncateDateFunction {

    private static final long ONE_HOUR = TimeUnit.HOURS.toMillis(1L);

    public static Timestamp truncateToHours(Timestamp timestamp) {

        return new Timestamp((timestamp.getTime() / ONE_HOUR) * ONE_HOUR);
    }
}
