package ru.silvmike.interview.anymind.public_wallet.rest.converter;

/**
 * Inteface for any bidirectional converter.
 *
 * @param <A> one type
 * @param <B> another type
 */
public interface Converter<A, B> {

    /**
     * Converts {@link B} to {@link A}.
     *
     * @param b element of type {@link B}
     * @return {@link A}
     */
    A fromB(B b);

    /**
     * Converts {@link A} to {@link B}.
     *
     * @param a element of type {@link A}
     * @return {@link B}
     */
    B fromA(A a);
}
