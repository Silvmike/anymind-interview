package ru.silvmike.interview.anymind.public_wallet.rest.error.advice;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.silvmike.interview.anymind.public_wallet.rest.ValidationException;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.BaseResponse;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.Error;

import static java.util.Collections.singletonList;

/**
 * Controller advice defining exception handler for all the instances of {@link ValidationException}.
 * <p>
 * Applicable only for controllers annotated by {@link ValidationExceptionHandling}.
 *
 * @see ValidationExceptionHandling
 */
@ControllerAdvice(annotations = ValidationExceptionHandling.class)
@Order(Ordered.HIGHEST_PRECEDENCE + 2)
public class ValidationControllerAdvice {

    /**
     * Handle all the {@link ValidationException} instances and build response objects based on it.
     *
     * @param exception exception for handling
     * @return response object for handled exception
     */
    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<?> handleError(ValidationException exception) {
        
        return ResponseEntity.badRequest()
            .body(
               new BaseResponse<>()
                    .setSuccess(false)
                    .setErrors(
                        singletonList(
                            new Error(exception.getMessage())
                        )
                    )
            );
    }
}
