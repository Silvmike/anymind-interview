package ru.silvmike.interview.anymind.public_wallet.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class WalletData {

    private BigDecimal amount;
    private OffsetDateTime dateTime;

}
