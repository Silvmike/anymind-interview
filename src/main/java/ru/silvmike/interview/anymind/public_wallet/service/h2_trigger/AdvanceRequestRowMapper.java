package ru.silvmike.interview.anymind.public_wallet.service.h2_trigger;

import org.springframework.jdbc.core.RowMapper;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

class AdvanceRequestRowMapper implements RowMapper<AdvanceRequest> {

    @Override
    public AdvanceRequest mapRow(ResultSet resultSet, int i) throws SQLException {
        return new AdvanceRequest(
            resultSet.getBigDecimal("AMOUNT"),
            resultSet.getObject("CREATED_AT", OffsetDateTime.class)
                     .withOffsetSameLocal(ZoneOffset.UTC)
        );
    }
}
