package ru.silvmike.interview.anymind.public_wallet.service.h2_trigger;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class H2TriggerSQLProvider {

    @Value("${sql.increment}")
    private String increment;

    @Value("${sql.summary}")
    private String summary;

    @Value("${sql.clean_history}")
    private String cleanHistory;

    @Value("${sql.clean_balance}")
    private String cleanBalance;

}
