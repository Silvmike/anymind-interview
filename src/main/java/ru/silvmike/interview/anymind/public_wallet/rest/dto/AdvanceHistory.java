package ru.silvmike.interview.anymind.public_wallet.rest.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class AdvanceHistory {

    private List<Advance> history;

}
