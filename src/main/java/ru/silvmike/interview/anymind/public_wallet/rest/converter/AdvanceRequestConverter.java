package ru.silvmike.interview.anymind.public_wallet.rest.converter;

import ru.silvmike.interview.anymind.public_wallet.rest.dto.Advance;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;

import java.util.Objects;

/**
 * Converts between {@link Advance} and {@link AdvanceRequest}.
 */
public class AdvanceRequestConverter implements Converter<Advance, AdvanceRequest> {

    @Override
    public Advance fromB(AdvanceRequest request) {
        Objects.requireNonNull(request, "'request' is required for this transformation!");

        return new Advance(request.getAmount(), request.getDatetime());
    }

    @Override
    public AdvanceRequest fromA(Advance advance) {
        Objects.requireNonNull(advance, "'advance' is required for this transformation!");

        return new AdvanceRequest(advance.getAmount(), advance.getDatetime());
    }
}
