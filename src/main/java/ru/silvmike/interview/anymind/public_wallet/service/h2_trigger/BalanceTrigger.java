package ru.silvmike.interview.anymind.public_wallet.service.h2_trigger;

import org.h2.api.Trigger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.function.Supplier;

/**
 * Inserts history into balance history table.
 */
public class BalanceTrigger implements Trigger {

    private static final Object monitor = new Object();
    private static Supplier<OffsetDateTime> currentTimeSupplier = OffsetDateTime::now;

    @Override
    public void init(
            Connection conn,
            String schemaName,
            String triggerName,
            String tableName,
            boolean before,
            int type) {
        // do nothing
    }

    @Override
    public void fire(Connection conn, Object[] oldRow, Object[] newRow) throws SQLException {

        synchronized (monitor) {
            try (
                PreparedStatement stmt = conn.prepareStatement(
                    "INSERT INTO BALANCE_HISTORY (CREATED_AT, AMOUNT) VALUES (?, ?)"
                )
            ) {
                stmt.setObject(1, currentTimeSupplier.get().withOffsetSameLocal(ZoneOffset.UTC));
                stmt.setObject(2, newRow[1]);
                stmt.executeUpdate();
            }
        }
    }

    /**
     * Allows to temporarily replace current time providing logic for test purposes while executing specified action.
     *
     * @param timeSupplier new time supplier
     * @param action action to execute
     */
    public static void executeWithOtherTimeSupplier(Supplier<OffsetDateTime> timeSupplier, Runnable action) {

        synchronized (monitor) {
            Supplier<OffsetDateTime> oldSupplier = currentTimeSupplier;
            try {

                currentTimeSupplier = timeSupplier;
                action.run();

            } finally {
                currentTimeSupplier = oldSupplier;
            }
        }
    }

    @Override
    public void close() {
        // do nothing
    }

    @Override
    public void remove() {
        // do nothing
    }
}
