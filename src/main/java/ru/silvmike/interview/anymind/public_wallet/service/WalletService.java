package ru.silvmike.interview.anymind.public_wallet.service;

import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.dto.WalletHistory;

import java.time.OffsetDateTime;

/**
 * Interface for services used to manipulate wallet state.
 */
public interface WalletService {

    /**
     * Updates wallet state.
     *
     * @param request update request
     */
    void advance(AdvanceRequest request);

    /**
     * Returns wallet advance history after given date.
     *
     * @param startInclusive start date of interval
     *
     * @return wallet history for a given period
     */
    default WalletHistory getHistory(OffsetDateTime startInclusive) {

        return getHistory(startInclusive, null);
    }


    /**
     * Returns wallet advance history for a given interval.
     *
     * @param startInclusive start date of interval
     * @param endInclusive end date of interval
     *
     * @return wallet history for a given period
     */
    WalletHistory getHistory(OffsetDateTime startInclusive, OffsetDateTime endInclusive);


}
