package ru.silvmike.interview.anymind.public_wallet.rest.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Error {

    private final String message;

}
