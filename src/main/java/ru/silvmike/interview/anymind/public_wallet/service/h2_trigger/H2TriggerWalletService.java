package ru.silvmike.interview.anymind.public_wallet.service.h2_trigger;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import ru.silvmike.interview.anymind.public_wallet.service.AbstractWalletService;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.dto.WalletHistory;

import java.sql.Types;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

public class H2TriggerWalletService extends AbstractWalletService {

    private final RowMapper<AdvanceRequest> rowMapper;
    private final NamedParameterJdbcTemplate jt;
    private final H2TriggerSQLProvider sql;

    public H2TriggerWalletService(NamedParameterJdbcTemplate jt, H2TriggerSQLProvider sql) {

        this.rowMapper = new AdvanceRequestRowMapper();
        this.jt = jt;
        this.sql = sql;
    }

    @Override
    public void advance(AdvanceRequest request) {

        this.jt.update(
            sql.getIncrement(),
            new MapSqlParameterSource()
                .addValue("advance", request.getAmount())
        );
    }

    @Override
    protected List<AdvanceRequest> doGetHistory(OffsetDateTime startInclusive, OffsetDateTime endInclusive) {

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource = parameterSource.addValue("left", startInclusive);
        if (endInclusive != null) {
            parameterSource = parameterSource.addValue("right", endInclusive);
        } else {
            parameterSource = parameterSource.addValue("right", null, Types.NULL);
        }

        return this.jt.query(
                sql.getSummary(),
                parameterSource,
                rowMapper
            );
    }

    @Transactional
    public void clear() {

        this.jt.update(sql.getCleanHistory(), Collections.emptyMap());

        BalanceTrigger.executeWithOtherTimeSupplier(
            () -> OffsetDateTime.now().withYear(2019),
            () -> this.jt.update(sql.getCleanBalance(), Collections.emptyMap())
        );
    }
}
