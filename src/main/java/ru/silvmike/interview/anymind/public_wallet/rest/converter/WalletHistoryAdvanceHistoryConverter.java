package ru.silvmike.interview.anymind.public_wallet.rest.converter;

import ru.silvmike.interview.anymind.public_wallet.rest.dto.Advance;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.AdvanceHistory;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.dto.WalletHistory;

import java.util.Objects;
import java.util.stream.Collectors;

public class WalletHistoryAdvanceHistoryConverter implements Converter<AdvanceHistory, WalletHistory> {

    private final Converter<Advance, AdvanceRequest> advanceRequestConverter;

    public WalletHistoryAdvanceHistoryConverter(Converter<Advance, AdvanceRequest> advanceRequestConverter) {
        this.advanceRequestConverter = advanceRequestConverter;
    }

    @Override
    public AdvanceHistory fromB(WalletHistory walletHistory) {
        Objects.requireNonNull(walletHistory, "'walletHistory' is required for this transformation!");

        return new AdvanceHistory()
            .setHistory(
                walletHistory.getHistory()
                    .stream()
                    .map(advanceRequestConverter::fromB)
                    .collect(Collectors.toList())
            );
    }

    @Override
    public WalletHistory fromA(AdvanceHistory advanceHistory) {
        throw new UnsupportedOperationException("not implemented!");
    }
}
