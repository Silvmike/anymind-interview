package ru.silvmike.interview.anymind.public_wallet.service.dto;

import lombok.Data;

import java.util.List;

@Data
public class WalletHistory {

    private final List<AdvanceRequest> history;

}
