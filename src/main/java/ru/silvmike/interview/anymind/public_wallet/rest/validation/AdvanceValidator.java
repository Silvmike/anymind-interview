package ru.silvmike.interview.anymind.public_wallet.rest.validation;

import ru.silvmike.interview.anymind.public_wallet.rest.ValidationException;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.Advance;

import java.math.BigDecimal;

/**
 * {@link Advance} validator.
 */
public class AdvanceValidator implements Validator<Advance> {

    @Override
    public void validate(Advance object) throws ValidationException {

        if (object == null) throw new ValidationException("Request mustn't be null!");

        validateAmount(object);
        validateDatetime(object);

    }

    private void validateDatetime(Advance object) {
        if (object.getDatetime() == null) throw new ValidationException("'datetime' is required!");
        if (object.getDatetime().getYear() < 2020) {
            throw new ValidationException("'datetime's year must be after 2019!");
        }
    }

    private void validateAmount(Advance object) {
        if (object.getAmount() == null) throw new ValidationException("'amount' is required!");
        if (object.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new ValidationException("'amount' must be greater than zero!");
        }
    }
}
