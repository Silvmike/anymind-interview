package ru.silvmike.interview.anymind.public_wallet.service.stub;

import ru.silvmike.interview.anymind.public_wallet.service.AbstractWalletService;
import ru.silvmike.interview.anymind.public_wallet.service.WalletService;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.dto.WalletHistory;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * <pre>
 * Stub {@link WalletService} implementation based on tree map.
 * Thread-safe.
 * </pre>
 */
public class StubWalletService extends AbstractWalletService {

    private final Object monitor = new Object();
    private final TreeMap<OffsetDateTime, AdvanceRequest> history = new TreeMap<>();

    public StubWalletService() {
        clear();
    }

    @Override
    public void advance(AdvanceRequest request) {

        OffsetDateTime index = extractIndexValue(request.getDatetime());
        OffsetDateTime prevIndex = extractIndexValue(request.getDatetime().minus(1L, ChronoUnit.HOURS));

        synchronized (monitor) {
            AdvanceRequest advanceRequest = history.computeIfAbsent(
                index,
                (elem) ->
                    new AdvanceRequest()
                        .setAmount(
                            history.computeIfAbsent(
                                prevIndex,
                                (innerElem) -> new AdvanceRequest(BigDecimal.ZERO, index)
                            ).getAmount()
                        ).setDatetime(index)
            );

            advanceRequest.setAmount(advanceRequest.getAmount().add(request.getAmount()));
        }
    }

    @Override
    public WalletHistory getHistory(OffsetDateTime startInclusive) {

        synchronized (monitor) {
            List<AdvanceRequest> requests =
                new ArrayList<>(history.tailMap(extractIndexValue(startInclusive), true).values());

            return new WalletHistory(requests);
        }
    }

    @Override
    protected List<AdvanceRequest> doGetHistory(OffsetDateTime startInclusive, OffsetDateTime endInclusive) {

        synchronized (monitor) {
            return new ArrayList<>(
                history.tailMap(startInclusive, true)
                       .headMap(endInclusive, true)
                       .values()
            );
        }
    }

    public void clear() {

        synchronized (monitor) {
            this.history.clear();
            advance(new AdvanceRequest(BigDecimal.ZERO, OffsetDateTime.now()));
        }
    }
}