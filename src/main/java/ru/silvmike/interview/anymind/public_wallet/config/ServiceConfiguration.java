package ru.silvmike.interview.anymind.public_wallet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import ru.silvmike.interview.anymind.public_wallet.Profiles;
import ru.silvmike.interview.anymind.public_wallet.service.WalletService;
import ru.silvmike.interview.anymind.public_wallet.service.stub.StubWalletService;

@Import(H2TriggerServiceConfiguration.class)
@Configuration
public class ServiceConfiguration {

    @Profile(Profiles.STUB)
    @Bean
    public WalletService stubWalletService() {
        return new StubWalletService();
    }
}
