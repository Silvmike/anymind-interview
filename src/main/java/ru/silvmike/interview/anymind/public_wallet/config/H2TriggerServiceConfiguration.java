package ru.silvmike.interview.anymind.public_wallet.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.silvmike.interview.anymind.public_wallet.Profiles;
import ru.silvmike.interview.anymind.public_wallet.service.WalletService;
import ru.silvmike.interview.anymind.public_wallet.service.h2_trigger.H2TriggerSQLProvider;
import ru.silvmike.interview.anymind.public_wallet.service.h2_trigger.H2TriggerWalletService;

import javax.sql.DataSource;

@Profile({Profiles.H2_TRIGGER, Profiles.H2_TRIGGER_STUB})
@PropertySource("classpath:queries-triggered.xml")
@Configuration
@Import(DataSourceConfiguration.class)
@EnableTransactionManagement
public class H2TriggerServiceConfiguration {

    @Bean
    public WalletService h2WalletService(
        @Qualifier(DataSourceConfiguration.WALLET_DATA_SOURCE) DataSource dataSource,
        H2TriggerSQLProvider sqlProvider
    ) {
        return new H2TriggerWalletService(
            new NamedParameterJdbcTemplate(dataSource),
            sqlProvider
        );
    }

    @Bean
    public H2TriggerSQLProvider sqlProvider() {
        return new H2TriggerSQLProvider();
    }

    @Bean
    public TransactionManager transactionManager(
            @Qualifier(DataSourceConfiguration.WALLET_DATA_SOURCE) DataSource dataSource) {

        return new DataSourceTransactionManager(dataSource);
    }
}
