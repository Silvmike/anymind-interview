package ru.silvmike.interview.anymind.public_wallet.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.flyway.FlywayDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

    public static final String WALLET_DATA_SOURCE = "walletDataSource";

    @FlywayDataSource
    @Bean(WALLET_DATA_SOURCE)
    public DataSource walletDataSource(HikariConfig config) {
        return new HikariDataSource(config);
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.wallet.hikari")
    public HikariConfig walletHikariConfig() {
        return new HikariConfig();
    }

}
