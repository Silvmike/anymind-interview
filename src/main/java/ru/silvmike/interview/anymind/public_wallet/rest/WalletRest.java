package ru.silvmike.interview.anymind.public_wallet.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.silvmike.interview.anymind.public_wallet.rest.converter.Converter;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.Advance;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.AdvanceHistory;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.BaseResponse;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.HistoryRequest;
import ru.silvmike.interview.anymind.public_wallet.rest.error.advice.ValidationExceptionHandling;
import ru.silvmike.interview.anymind.public_wallet.rest.validation.Validator;
import ru.silvmike.interview.anymind.public_wallet.service.WalletService;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.dto.WalletHistory;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@RestController
@RequestMapping(
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
)
@ValidationExceptionHandling
public class WalletRest {

    public static final String ADVANCE_URL = "/advance";
    public static final String HISTORY_URL = "/history";

    private final WalletService walletService;
    private final Converter<Advance, AdvanceRequest> advanceRequestConverter;
    private final Converter<AdvanceHistory, WalletHistory> walletHistoryConverter;
    private final Validator<Advance> advanceValidator;
    private final Validator<HistoryRequest> historyRequestValidator;

    public WalletRest(
            WalletService walletService,
            Converter<Advance, AdvanceRequest> advanceRequestConverter,
            Converter<AdvanceHistory, WalletHistory> walletHistoryConverter,
            Validator<Advance> advanceValidator,
            Validator<HistoryRequest> historyRequestValidator) {

        this.walletService = walletService;
        this.advanceRequestConverter = advanceRequestConverter;
        this.walletHistoryConverter = walletHistoryConverter;
        this.advanceValidator = advanceValidator;
        this.historyRequestValidator = historyRequestValidator;
    }

    @PostMapping(path = ADVANCE_URL)
    public BaseResponse<Void> advance(@RequestBody Advance request) {
        validateAdvanceRequest(request);
        normalizeAdvanceRequest(request);

        walletService.advance(advanceRequestConverter.fromA(request));
        return new BaseResponse<>();
    }

    private void normalizeAdvanceRequest(Advance request) {

        request.setDatetime(normalizeOffsetDateTime(request.getDatetime()));
    }

    private OffsetDateTime normalizeOffsetDateTime(OffsetDateTime dateTime) {
        return dateTime.withOffsetSameLocal(ZoneOffset.UTC);
    }

    private void validateAdvanceRequest(Advance request) {
        advanceValidator.validate(request);
    }

    @PostMapping(HISTORY_URL)
    public BaseResponse<AdvanceHistory> getHistory(@RequestBody HistoryRequest historyRequest) {
        validateHistoryRequest(historyRequest);
        normalizeHistoryRequest(historyRequest);

        return new BaseResponse<AdvanceHistory>()
            .setBody(
                walletHistoryConverter.fromB(
                    walletService.getHistory(
                        historyRequest.getStartDatetime(),
                        historyRequest.getEndDatetime()
                    )
                )
            );
    }

    private void normalizeHistoryRequest(HistoryRequest request) {

        request.setStartDatetime(normalizeOffsetDateTime(request.getStartDatetime()));
        request.setEndDatetime(normalizeOffsetDateTime(request.getEndDatetime()));
    }

    private void validateHistoryRequest(HistoryRequest historyRequest) {
        historyRequestValidator.validate(historyRequest);
    }
}
