package ru.silvmike.interview.anymind.public_wallet.rest.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class BaseResponse<ResponseType> {

    private boolean success = true;
    private ResponseType body;

    private List<Error> errors;

}
