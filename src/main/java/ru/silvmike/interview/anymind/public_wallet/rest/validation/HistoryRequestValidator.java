package ru.silvmike.interview.anymind.public_wallet.rest.validation;

import ru.silvmike.interview.anymind.public_wallet.rest.ValidationException;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.HistoryRequest;

import java.time.OffsetDateTime;

/**
 * {@link HistoryRequest} validator.
 */
public class HistoryRequestValidator implements Validator<HistoryRequest> {

    @Override
    public void validate(HistoryRequest object) throws ValidationException {

        if (object == null) throw new ValidationException("Request mustn't be null!");
        validateDatetime(object.getStartDatetime(), "startDatetime");
        validateDatetime(object.getEndDatetime(), "endDatetime");
    }

    private void validateDatetime(OffsetDateTime object, String name) {
        if (object == null) {
            throw new ValidationException(String.format("'%s' is required!", name));
        }
        if (object.getYear() < 2020) {
            throw new ValidationException(String.format("'%s's year must be after 2019!", name));
        }
    }
}
