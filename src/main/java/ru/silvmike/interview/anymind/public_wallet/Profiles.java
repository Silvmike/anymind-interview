package ru.silvmike.interview.anymind.public_wallet;

public interface Profiles {

    String STUB = "STUB";
    String H2_TRIGGER = "H2_TRIGGER";
    String H2_TRIGGER_STUB = "H2_TRIGGER_STUB";
}
