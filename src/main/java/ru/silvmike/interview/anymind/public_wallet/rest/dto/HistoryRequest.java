package ru.silvmike.interview.anymind.public_wallet.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class HistoryRequest {

    private OffsetDateTime startDatetime;
    private OffsetDateTime endDatetime;

}
