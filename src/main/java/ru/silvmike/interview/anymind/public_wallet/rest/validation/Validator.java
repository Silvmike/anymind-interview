package ru.silvmike.interview.anymind.public_wallet.rest.validation;

import ru.silvmike.interview.anymind.public_wallet.rest.ValidationException;

/**
 * Validator interface.
 *
 * @param <T> applicable object
 */
public interface Validator<T> {

    /**
     * Validates object.
     *
     * @param object object of validation
     * @throws ValidationException when object is invalid
     */
    void validate(T object) throws ValidationException;

}
