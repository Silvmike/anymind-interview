package ru.silvmike.interview.anymind.public_wallet.service;

import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.dto.WalletHistory;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Abstract {@link WalletService} capable of completing incomplete history responses.
 */
public abstract class AbstractWalletService implements WalletService {

    @Override
    public final WalletHistory getHistory(OffsetDateTime startInclusive, OffsetDateTime endInclusive) {

        return new WalletHistory(
            addMissingHours(
                extractIndexValue(startInclusive),
                extractIndexValue(endInclusive),
                doGetHistory(
                    extractIndexValue(startInclusive),
                    extractIndexValue(endInclusive)
                )
            )
        );
    }

    protected abstract List<AdvanceRequest> doGetHistory(OffsetDateTime startInclusive, OffsetDateTime endInclusive);

    protected List<AdvanceRequest> addMissingHours(
            OffsetDateTime startInclusive, OffsetDateTime endInclusive, List<AdvanceRequest> result) {

        if (endInclusive != null) {

            TreeMap<OffsetDateTime, AdvanceRequest> existing = new TreeMap<>();
            result.forEach(q ->
                existing.put(extractIndexValue(q.getDatetime()), q)
            );

            OffsetDateTime current = startInclusive;
            AdvanceRequest lastRequest = null;
            while (current.compareTo(endInclusive) <= 0) {

                if (existing.containsKey(current)) {
                    lastRequest = existing.get(current);
                } else {
                    if (lastRequest != null) {
                        existing.put(current, new AdvanceRequest(lastRequest.getAmount(), current));
                    } else {
                        existing.put(current, new AdvanceRequest(BigDecimal.ZERO, current));
                    }
                }
                current = current.plusHours(1L);
            }
            return new ArrayList<>(existing.values());
        }
        return result;
    }

    protected OffsetDateTime extractIndexValue(OffsetDateTime value) {
        if (value == null) return null;
        return value.withOffsetSameLocal(ZoneOffset.UTC).truncatedTo(ChronoUnit.HOURS);
    }
}
