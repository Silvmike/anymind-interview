CREATE TABLE IF NOT EXISTS BALANCE
(
    ID BIGINT AUTO_INCREMENT PRIMARY KEY,
    AMOUNT DECIMAL(19, 4) NOT NULL
);

CREATE TABLE IF NOT EXISTS BALANCE_HISTORY
(
    ID BIGINT AUTO_INCREMENT PRIMARY KEY,
    CREATED_AT TIMESTAMP WITH TIME ZONE NOT NULL,
    AMOUNT DECIMAL(19, 4) NOT NULL
);

INSERT INTO BALANCE(AMOUNT) VALUES(0);

CREATE INDEX BALANCE_HISTORY_CREATED_AT_IDX ON BALANCE_HISTORY(CREATED_AT);

CREATE TRIGGER BALANCE_TG
BEFORE UPDATE
ON BALANCE
FOR EACH ROW
CALL "ru.silvmike.interview.anymind.public_wallet.service.h2_trigger.BalanceTrigger";

CREATE ALIAS TRUNCATE_TO_HOURS
FOR "ru.silvmike.interview.anymind.public_wallet.service.h2_trigger.TruncateDateFunction.truncateToHours";