package ru.silvmike.interview.anymind.public_wallet.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ru.silvmike.interview.anymind.public_wallet.Profiles;
import ru.silvmike.interview.anymind.public_wallet.config.RootConfiguration;

@SpringBootTest(
	classes = { Application.class, RootConfiguration.class }
)
@ActiveProfiles(Profiles.H2_TRIGGER)
class ApplicationTests {

	@Test
	void contextLoads() {
	}

}
