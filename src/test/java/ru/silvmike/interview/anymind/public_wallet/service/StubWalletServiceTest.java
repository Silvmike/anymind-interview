package ru.silvmike.interview.anymind.public_wallet.service;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.stub.StubWalletService;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import static org.assertj.core.data.Index.atIndex;

public abstract class StubWalletServiceTest {

    private final StubWalletService walletService = new StubWalletService();

    @BeforeEach
    public void setUp() {
        clear();
    }

    @Test
    public void ensureZeroBalanceAtTheBeginning() {

        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime hourAgo = now.minusHours(1L);

        Assertions.assertThat(walletService.getHistory(hourAgo, now).getHistory())
            .isNotEmpty()
            .allMatch(request -> request.getAmount().equals(BigDecimal.ZERO));

    }

    @Test
    public void advanceChangesAmount() {

        OffsetDateTime afterTestStart = OffsetDateTime.now().plusHours(1L);

        Assertions.assertThat(walletService.getHistory(afterTestStart).getHistory()).isEmpty();

        AdvanceRequest first = new AdvanceRequest(BigDecimal.ONE, afterTestStart);
        AdvanceRequest second = new AdvanceRequest(BigDecimal.ONE, afterTestStart);
        walletService.advance(first);
        walletService.advance(second);

        Assertions.assertThat(walletService.getHistory(afterTestStart).getHistory())
            .hasSize(1)
            .has(new Condition<>(
                    data -> data.getAmount()
                                .compareTo(first.getAmount().add(second.getAmount())) == 0,
                    "Expected 2.0 amount after the second advance"
                ),
                atIndex(0)
            );
    }

    @Test
    public void historyRespectsBothBounds() {

        OffsetDateTime testStart = OffsetDateTime.now().plusHours(1L);
        OffsetDateTime left = testStart.plusHours(1L);
        OffsetDateTime right = left.plusHours(1L);

        AdvanceRequest first = new AdvanceRequest(BigDecimal.ONE, left);
        AdvanceRequest second = new AdvanceRequest(BigDecimal.ONE, right);
        walletService.advance(first);
        walletService.advance(second);

        Assertions.assertThat(walletService.getHistory(left, right).getHistory())
            .hasSize(2)
            .has(new Condition<>(
                    data -> data.getAmount().compareTo(BigDecimal.ONE) == 0,
                    "Expected 1.0 after the first advance"
                ),
                atIndex(0)
            )
            .has(new Condition<>(
                    data -> data.getAmount()
                                .compareTo(first.getAmount().add(second.getAmount())) == 0,
                    "Expected 2.0 after the second advance"
                ),
                atIndex(1)
            );

        Assertions.assertThat(walletService.getHistory(left, left).getHistory())
            .hasSize(1)
            .has(new Condition<>(
                    data -> data.getAmount().compareTo(BigDecimal.ONE) == 0,
                    "Expected 1.0 after the first advance"
                ),
                atIndex(0)
            );

    }

    @Test
    public void historyRespectsLeftBound() {

        OffsetDateTime testStart = OffsetDateTime.now().plusHours(1L);
        OffsetDateTime left = testStart.plusHours(1L);
        OffsetDateTime right = left.plusHours(1L);

        AdvanceRequest first = new AdvanceRequest(BigDecimal.ONE, left);
        AdvanceRequest second = new AdvanceRequest(BigDecimal.ONE, right);
        walletService.advance(first);
        walletService.advance(second);

        Assertions.assertThat(walletService.getHistory((right)).getHistory())
            .hasSize(1)
            .has(new Condition<>(
                    data -> data.getAmount()
                                .compareTo(first.getAmount().add(second.getAmount())) == 0,
                    "Expected 2.0 after the first advance"
                ),
                atIndex(0)
            );

    }

    private void clear() {
        walletService.clear();
    }

}
