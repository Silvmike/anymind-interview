package ru.silvmike.interview.anymind.public_wallet.rest.converter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.silvmike.interview.anymind.public_wallet.rest.ValidationException;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.Advance;
import ru.silvmike.interview.anymind.public_wallet.rest.validation.AdvanceValidator;
import ru.silvmike.interview.anymind.public_wallet.rest.validation.Validator;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class AdvanceValidatorTest {

    private static final String DATETIME_YEAR_MUST_BE_AFTER_2019 = "'datetime's year must be after 2019!";
    private static final String AMOUNT_MUST_BE_GREATER_THAN_ZERO = "'amount' must be greater than zero!";

    private final Validator<Advance> validator = new AdvanceValidator();

    static Stream<Arguments> testData() {

        return Stream.of(
            arguments(null, "Request mustn't be null!"),
            data(null, OffsetDateTime.now(), "'amount' is required!"),
            data(BigDecimal.ONE, null, "'datetime' is required!"),
            data(BigDecimal.ZERO, OffsetDateTime.now(), AMOUNT_MUST_BE_GREATER_THAN_ZERO),
            data(BigDecimal.ONE.negate(), OffsetDateTime.now(), AMOUNT_MUST_BE_GREATER_THAN_ZERO),
            data(BigDecimal.ONE, OffsetDateTime.now().withYear(2010), DATETIME_YEAR_MUST_BE_AFTER_2019),
            data(BigDecimal.ONE, OffsetDateTime.now().withYear(2019), DATETIME_YEAR_MUST_BE_AFTER_2019)
        );
    }

    private static Arguments data(BigDecimal amount, OffsetDateTime datetime, String expectedMessage) {
        return arguments(new Advance(amount, datetime), expectedMessage);
    }

    @MethodSource("testData")
    @ParameterizedTest
    void testValidation(Advance advance, String expectedMessage) {

        assertThatCode(() -> validator.validate(advance))
            .as("Should throw exception on null object")
            .isInstanceOf(ValidationException.class)
            .extracting(Throwable::getMessage)
            .as("Validation message was different")
            .isEqualTo(expectedMessage);
    }

    @Test
    void testValidationOK() {

        Advance advance = new Advance(BigDecimal.ONE, OffsetDateTime.now());
        assertThatCode(() -> validator.validate(advance))
            .as("Validation wasn't supposed to throw on valid object!")
            .doesNotThrowAnyException();

    }
}
