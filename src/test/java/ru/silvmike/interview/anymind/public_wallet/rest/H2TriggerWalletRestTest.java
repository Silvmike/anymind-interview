package ru.silvmike.interview.anymind.public_wallet.rest;

import org.springframework.test.context.ActiveProfiles;
import ru.silvmike.interview.anymind.public_wallet.Profiles;

@ActiveProfiles(Profiles.H2_TRIGGER_STUB)
public class H2TriggerWalletRestTest extends BaseWalletRestTest {
}
