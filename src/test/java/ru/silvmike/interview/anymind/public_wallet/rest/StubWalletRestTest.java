package ru.silvmike.interview.anymind.public_wallet.rest;

import org.springframework.test.context.ActiveProfiles;
import ru.silvmike.interview.anymind.public_wallet.Profiles;

@ActiveProfiles(Profiles.STUB)
public class StubWalletRestTest extends BaseWalletRestTest {
}
