package ru.silvmike.interview.anymind.public_wallet.rest.converter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.silvmike.interview.anymind.public_wallet.rest.ValidationException;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.HistoryRequest;
import ru.silvmike.interview.anymind.public_wallet.rest.validation.HistoryRequestValidator;
import ru.silvmike.interview.anymind.public_wallet.rest.validation.Validator;

import java.time.OffsetDateTime;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class HistoryRequestValidatorTest {

    private static final String START_DATETIME_YEAR_MUST_BE_AFTER_2019 = "'startDatetime's year must be after 2019!";
    private static final String END_DATETIME_YEAR_MUST_BE_AFTER_2019 = "'endDatetime's year must be after 2019!";

    private final Validator<HistoryRequest> validator = new HistoryRequestValidator();

    static Stream<Arguments> testData() {

        final OffsetDateTime wrongDateOne = OffsetDateTime.now().withYear(2010);
        final OffsetDateTime wrongDateTwo = OffsetDateTime.now().withYear(2010);

        return Stream.of(
            arguments(null, "Request mustn't be null!"),
            data(OffsetDateTime.now(), null, "'endDatetime' is required!"),
            data(wrongDateOne, null, START_DATETIME_YEAR_MUST_BE_AFTER_2019),
            data(wrongDateTwo, null, START_DATETIME_YEAR_MUST_BE_AFTER_2019),
            data(null, OffsetDateTime.now(), "'startDatetime' is required!"),
            data(OffsetDateTime.now(), wrongDateOne, END_DATETIME_YEAR_MUST_BE_AFTER_2019),
            data(OffsetDateTime.now(), wrongDateTwo, END_DATETIME_YEAR_MUST_BE_AFTER_2019)
        );
    }

    private static Arguments data(OffsetDateTime startDatetime, OffsetDateTime endDatetime, String expectedMessage) {
        return arguments(new HistoryRequest(startDatetime, endDatetime), expectedMessage);
    }

    @MethodSource("testData")
    @ParameterizedTest
    void testValidation(HistoryRequest advance, String expectedMessage) {

        assertThatCode(() -> validator.validate(advance))
            .as("Should throw exception on null object")
            .isInstanceOf(ValidationException.class)
            .extracting(Throwable::getMessage)
            .as("Validation message was different")
            .isEqualTo(expectedMessage);
    }

    @Test
    void testValidationOK() {

        HistoryRequest advance = new HistoryRequest(OffsetDateTime.now(), OffsetDateTime.now());
        assertThatCode(() -> validator.validate(advance))
            .as("Validation wasn't supposed to throw on valid object!")
            .doesNotThrowAnyException();

    }
}
