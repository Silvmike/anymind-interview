package ru.silvmike.interview.anymind.public_wallet.service;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import ru.silvmike.interview.anymind.public_wallet.Profiles;
import ru.silvmike.interview.anymind.public_wallet.app.Application;
import ru.silvmike.interview.anymind.public_wallet.config.RootConfiguration;
import ru.silvmike.interview.anymind.public_wallet.service.dto.AdvanceRequest;
import ru.silvmike.interview.anymind.public_wallet.service.h2_trigger.BalanceTrigger;
import ru.silvmike.interview.anymind.public_wallet.service.h2_trigger.H2TriggerWalletService;

import java.math.BigDecimal;
import java.time.OffsetDateTime;

import static org.assertj.core.data.Index.atIndex;

@SpringBootTest(
    classes = { Application.class, RootConfiguration.class },
    webEnvironment = SpringBootTest.WebEnvironment.NONE
)
@ActiveProfiles(Profiles.H2_TRIGGER_STUB)
public class H2TriggerWalletServiceTest {

    @Autowired
    private H2TriggerWalletService walletService;

    @BeforeEach
    public void setUp() {
        clear();
    }

    @Test
    public void ensureZeroBalanceAtTheBeginning() {

        OffsetDateTime now = OffsetDateTime.now();
        OffsetDateTime hourAgo = now.minusHours(1L);

        Assertions.assertThat(walletService.getHistory(hourAgo, now).getHistory())
            .isNotEmpty()
            .allMatch(request -> request.getAmount().equals(BigDecimal.ZERO));

    }

    @Test
    public void advanceChangesAmount() {

        OffsetDateTime afterTestStart = OffsetDateTime.now().plusHours(1L);

        Assertions.assertThat(walletService.getHistory(afterTestStart).getHistory()).isEmpty();

        AdvanceRequest first = new AdvanceRequest(BigDecimal.ONE, afterTestStart);
        AdvanceRequest second = new AdvanceRequest(BigDecimal.ONE, afterTestStart);
        BalanceTrigger.executeWithOtherTimeSupplier(
            () -> afterTestStart,
            () -> {
                walletService.advance(first);
                walletService.advance(second);
            }
        );

        Assertions.assertThat(walletService.getHistory(afterTestStart).getHistory())
            .hasSize(1)
            .has(new Condition<>(
                    data -> data.getAmount()
                                .compareTo(first.getAmount().add(second.getAmount())) == 0,
                    "Expected 2.0 amount after the second advance"
                ),
                atIndex(0)
            );
    }

    @Test
    public void historyRespectsBothBounds() {

        OffsetDateTime testStart = OffsetDateTime.now().plusHours(1L);
        OffsetDateTime left = testStart.plusHours(1L);
        OffsetDateTime right = left.plusHours(1L);

        AdvanceRequest first = new AdvanceRequest(BigDecimal.ONE, left);
        AdvanceRequest second = new AdvanceRequest(BigDecimal.ONE, right);
        BalanceTrigger.executeWithOtherTimeSupplier(
            () -> left,
            () -> walletService.advance(first)
        );
        BalanceTrigger.executeWithOtherTimeSupplier(
            () -> right,
            () -> walletService.advance(second)
        );

        Assertions.assertThat(walletService.getHistory(left, right).getHistory())
            .hasSize(2)
            .has(new Condition<>(
                    data -> data.getAmount().compareTo(BigDecimal.ONE) == 0,
                    "Expected 1.0 after the first advance"
                ),
                atIndex(0)
            )
            .has(new Condition<>(
                    data -> data.getAmount()
                                .compareTo(first.getAmount().add(second.getAmount())) == 0,
                    "Expected 2.0 after the second advance"
                ),
                atIndex(1)
            );

        Assertions.assertThat(walletService.getHistory(left, left).getHistory())
            .hasSize(1)
            .has(new Condition<>(
                    data -> data.getAmount().compareTo(BigDecimal.ONE) == 0,
                    "Expected 1.0 after the first advance"
                ),
                atIndex(0)
            );

    }

    @Test
    public void historyRespectsLeftBound() {

        OffsetDateTime testStart = OffsetDateTime.now().plusHours(1L);
        OffsetDateTime left = testStart.plusHours(1L);
        OffsetDateTime right = left.plusHours(1L);

        AdvanceRequest first = new AdvanceRequest(BigDecimal.ONE, left);
        AdvanceRequest second = new AdvanceRequest(BigDecimal.ONE, right);
        BalanceTrigger.executeWithOtherTimeSupplier(
            () -> left,
            () -> walletService.advance(first)
        );
        BalanceTrigger.executeWithOtherTimeSupplier(
            () -> right,
            () -> walletService.advance(second)
        );

        Assertions.assertThat(walletService.getHistory(right).getHistory())
            .hasSize(1)
            .has(new Condition<>(
                    data -> data.getAmount()
                                .compareTo(first.getAmount().add(second.getAmount())) == 0,
                    "Expected 2.0 after the first advance"
                ),
                atIndex(0)
            );

    }

    private void clear() {
        walletService.clear();
    }
}
