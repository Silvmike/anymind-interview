package ru.silvmike.interview.anymind.public_wallet.rest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.web.reactive.server.WebTestClient;
import ru.silvmike.interview.anymind.public_wallet.app.Application;
import ru.silvmike.interview.anymind.public_wallet.config.RootConfiguration;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.Advance;
import ru.silvmike.interview.anymind.public_wallet.rest.dto.HistoryRequest;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.OffsetDateTime;

@SpringBootTest(
    classes = { Application.class, RootConfiguration.class },
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureWebTestClient
public abstract class BaseWalletRestTest {

    private static final String BASE_URL_TEMPLATE = "http://localhost:%d/%s";
    private static final long WEB_TEST_CLIENT_TIMEOUT_SECONDS = 5L;

    @LocalServerPort
    private int randomServerPort;

    @Test
    public void testAdvanceNoAmount() {

        testAdvanceValidation(
            new Advance()
                .setDatetime(OffsetDateTime.now()),
            "'amount' is required!"
        );
    }

    @Test
    public void testAdvanceBadAmount() {

        testAdvanceValidation(
            new Advance()
                .setAmount(BigDecimal.ONE.negate())
                .setDatetime(OffsetDateTime.now()),
            "'amount' must be greater than zero!"
        );
    }

    @Test
    public void testAdvanceNoDatetime() {

        testAdvanceValidation(
            new Advance().setAmount(BigDecimal.ONE),
            "'datetime' is required!"
        );
    }

    @Test
    public void testAdvanceBadDatetime() {

        OffsetDateTime wrongDatetime = OffsetDateTime.now().withYear(2010);
        testAdvanceValidation(
            new Advance()
                .setAmount(BigDecimal.ONE)
                .setDatetime(wrongDatetime),
            "'datetime's year must be after 2019!"
        );
    }

    private void testAdvanceValidation(Advance advance, String expectedMessage) {

        WebTestClient client = createClient(WalletRest.ADVANCE_URL);
        client.post()
            .bodyValue(advance)
            .exchange()
            .expectStatus().isBadRequest()
            .expectBody()
            .jsonPath("$.success").isEqualTo(false)
            .jsonPath("$.errors[0].message").isEqualTo(expectedMessage);
    }

    @Test
    public void testAdvance() {

        WebTestClient client = createClient(WalletRest.ADVANCE_URL);
        client.post()
            .bodyValue(
                new Advance()
                    .setAmount(new BigDecimal("1.07"))
                    .setDatetime(OffsetDateTime.now())
            )
            .exchange()
            .expectStatus().isOk()
            .expectBody().jsonPath("$.success").isEqualTo(true);

    }

    @Test
    public void testHistoryRequestNoStartDatetime() {

        testHistoryRequestValidation(
            new HistoryRequest(),
            "'startDatetime' is required!"
        );
    }

    @Test
    public void testHistoryRequestBadStartDatetime() {

        OffsetDateTime wrongDatetime = OffsetDateTime.now().withYear(2010);
        testHistoryRequestValidation(
            new HistoryRequest()
                .setStartDatetime(wrongDatetime)
                .setEndDatetime(OffsetDateTime.now()),
            "'startDatetime's year must be after 2019!"
        );
    }

    @Test
    public void testHistoryRequestNoEndDatetime() {

        testHistoryRequestValidation(
            new HistoryRequest().setStartDatetime(OffsetDateTime.now()),
            "'endDatetime' is required!"
        );
    }

    @Test
    public void testHistoryRequestBadEndDatetime() {

        OffsetDateTime wrongDatetime = OffsetDateTime.now().withYear(2010);
        testHistoryRequestValidation(
            new HistoryRequest()
                .setEndDatetime(wrongDatetime)
                .setStartDatetime(OffsetDateTime.now()),
            "'endDatetime's year must be after 2019!"
        );
    }

    private void testHistoryRequestValidation(HistoryRequest request, String expectedMessage) {

        WebTestClient client = createClient(WalletRest.HISTORY_URL);
        client.post()
            .bodyValue(request)
            .exchange()
            .expectStatus().isBadRequest()
            .expectBody()
            .jsonPath("$.success").isEqualTo(false)
            .jsonPath("$.errors[0].message").isEqualTo(expectedMessage);
    }

    @Test
    public void testGetHistory() {

        WebTestClient client = createClient(WalletRest.HISTORY_URL);
        client.post()
            .bodyValue(
                new HistoryRequest()
                    .setStartDatetime(OffsetDateTime.now())
                    .setEndDatetime(OffsetDateTime.now())
            )
            .exchange()
            .expectStatus().isOk()
            .expectBody().jsonPath("$.success").isEqualTo(true);

    }

    private WebTestClient createClient(String path) {

        return WebTestClient.bindToServer()
            .responseTimeout(Duration.ofSeconds(WEB_TEST_CLIENT_TIMEOUT_SECONDS))
            .baseUrl(String.format(BASE_URL_TEMPLATE, randomServerPort, path))
            .build();
    }
}
